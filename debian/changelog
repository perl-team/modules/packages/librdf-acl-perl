librdf-acl-perl (0.104-3) unstable; urgency=medium

  * Team upload.
  * Remove Makefile.old via debian/clean. (Closes: #1048967)

 -- gregor herrmann <gregoa@debian.org>  Thu, 07 Mar 2024 18:46:29 +0100

librdf-acl-perl (0.104-2) unstable; urgency=medium

  [ Florian Schlichting ]
  * Drop build-dependency on unused libmodule-signature-perl (closes: #783767)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/*: update URLs from {search,www}.cpan.org to MetaCPAN.
  * debian/*: update GitHub URLs to use HTTPS.

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Set upstream metadata fields: Bug-Database, Repository, Repository-
    Browse.
  * Set Testsuite header for perl package.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 17 Jun 2022 08:54:49 +0100

librdf-acl-perl (0.104-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 09 Jan 2021 17:18:38 +0100

librdf-acl-perl (0.104-1) unstable; urgency=medium

  [ upstream ]
  * New release.
    + Switch packaging to Dist::Inkt.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Jonas Smedegaard ]
  * Bump compatibility claim to Policy 3.9.5.
  * Update copyright info:
    + Drop Files sections for no longer included convenience code
      copies.
    + Extend coverage of packaging.
    + Extend coverage for main author.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 11 Sep 2014 10:14:59 +0200

librdf-acl-perl (0.103-1) unstable; urgency=low

  [ upstream ]
  * New release.
    + Stop bundling early paleolithic version of Scalar::Util.
      Closes: bug#713252.

  [ Jonas Smedegaard ]
  * List upstream issue tracker as contact, and metacpan.org URL as
    Homepage.
  * Update package relations:
    + Fix (build-)depend on libossp-uuid-perl (not virtual
      libdata-uuid-perl).
    + Relax to (build-)depend unversioned on cdbs: Needed version
      satisfied even in oldstable.
  * Update copyright file:
    + Fix use comment and license pseudo-sections in copyright file to
      obey silly restrictions of copyright format 1.0.
    + Quote license in comment.
    + Update copyright coverage for convenience code copies and main
      upstream author.
  * Bump debhelper compatibility level to 8.
  * Add README.source emphasizing control.in file as *not* a
    show-stopper for contributions, referring to wiki page for details.
  * Add git URL as alternate source.
  * Stop tracking md5sum of upstream tarball.
  * Bump packaging license to GPL-3+, and extend copyrigt coverage for
    myself to include current year.
  * Bump standards-version to 3.9.4.
  * Drop patch 1002: Build problem turned out to be (and fixed by now)
    in CDBS.

  [ Salvatore Bonaccorso ]
  * Use canonical anonscm.debian.org hostname in Vcs-Git URI.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 16 Jul 2013 18:53:46 +0200

librdf-acl-perl (0.102-1) unstable; urgency=low

  * New upstream release.
    + Extend copyright coverage to 2012.
    + Use Module::Package::RDF.
    + Drop RDF::TrineShortcuts dependency; use RDF::TrineX::Functions.

  * Update patches:
    + Drop patch 2001 disabling verification of source: Fixed upstream.
    + Refresh patch 1001.
    + Add patch 1002 to avoid auto-installing dependencies during build.
  * (Build-)depend on librdf-query-perl, librdf-query-client-perl and
    librdf-trinex-functions-perl (not librdf-trineshortcuts-perl or
    libxml-libxml-perl).
  * Update copyright file:
    + Update Files sections for included code copies of other projects.
    + Extend coverage of main project and Debian packaging.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 06 Jun 2012 00:17:43 +0200

librdf-acl-perl (0.101-2) unstable; urgency=low

  [ Jonas Smedegaard ]
  * Update copyright file:
    + Fix add License section for Artistic.
    + Shorten comments.
    + Quote licenses in comments.
    + Bump copyright format to final version 1.0.
    + Fix double-indent in Copyright fields as per Policy §5.6.13.
  * Update package relations:
    + Relax build-dependency on cdbs (needlessly tight).
    + Relax build-depend unversioned on debhelper and devscripts (needed
      versions satisfied even in oldstable).
  * Add patch 1001 to fix test to cope with authid sorting, and stop
  * suppressing regression test failures: should be reliable now.
  * Bump standards version to 3.9.3.
  * Use anonscm.debian.org for Vcs-Browser field.

  [ gregor herrmann ]
  * Remove debian/source/local-options: abort-on-upstream-changes and
    unapply-patches are default in dpkg-source since 1.16.1.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 21 May 2012 18:12:43 +0200

librdf-acl-perl (0.101-1) unstable; urgency=low

  * Initial packaging release.
    Closes: bug#626865.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 16 May 2011 04:13:01 +0200
